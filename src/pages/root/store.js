
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import axios from 'axios'
import router from '../../router'

export default new Vuex.Store({
  state: {
    pages: { },
    active: {
      now: '',
      elm: null
    },
    grid: {},
    yaml: `
    flexcenter: &flctr
      display: flex
      align-items: center
      justify-content: center
    grid:
      style:
        display: grid
        grid-template-columns: repeat(6,5vw)
        grid-template-rows: repeat(20,5vh)
        margin-left: 33.3vw
        #
      topbar:
        style:
          display: grid
          grid-column: 1 / span 6
          grid-row: 1
          grid-template-columns: repeat(10,10%)
          grid-template-rows: repeat(1,100%)
        content:
          - q4Zar
          - productions
          - TW!$T
          - CrYptCa$h
          - XCorp
          - N3TW0RK
          - HashWall
          - School
          - Playground
          - '*'
        content_style: *flctr
      topnav:
        style:
          display: grid
          grid-column: 1 / span 6
          grid-row: 3
          grid-template-columns: repeat(5,20%)
          grid-template-rows: repeat(1,100%)
        content:
          - test
          - test2
        content_style:
          <<: *flctr
          font-size: 150%
      sidenav:
        style:
          display: grid
          grid-column: 3
          grid-row: 4 / span 16
          grid-template-columns: repeat(1,100%)
          grid-template-rows: repeat(8,12.5%)
        content:
          - 一
          - 二
          - 三
          - 四
          - 五
          - 六
          - 七
          - 八
        content_style:
          <<: *flctr
          font-size: 150%
      homenav:
        style:
          display: grid
          grid-column: 4 / span 15
          grid-row: 4 / span 16
          grid-template-columns: repeat(4,25%)
          grid-template-rows: repeat(2,50%)
        content:
          - CURATORIAL PROJECTS
          - BUILDING COLLECTIONS
          - ART INVESTMENT
          - THE NEW PATRONS CLUB
          - PUBLIC PRIVATE
          - RESEARCH / EDUCATIONAL
          - PUBLISHING
          - PHILANTROPY
        content_style_grid:
          display: grid
          grid-template-rows: repeat(2,50%)
        content_style_center:
          <<: *flctr
      pages:
        styles:
          grid-column: 4 / span 15
          grid-row: 4 / span 16
          color: red
    `
  },
  mutations: {
    __init__(state){
      const yaml = require('js-yaml')
      state.grid = yaml.safeLoad(state.yaml).grid
      // console.log(state.grid)
    },
    ApiContactCreate(state, payload){
      axios.post('https://api.aendlyx.tech/contacts', payload)
      .then(rsp=>{
        // console.log(rsp.data)
      })
      router.push('/')
    },
    over(state, event){
      let idnum = event.target.id.split('-')[2]
      if (idnum) { //> sidenav or homenav
        let targets = [`grid-sidenav-${idnum}`, `grid-homenav-${idnum}-ico`, `grid-homenav-${idnum}-txt`]
        targets.forEach(element=>{
          let domitem = document.getElementById(element)
          //console.log(domitem)
          domitem.style.backgroundColor = 'black'
          domitem.style.color = 'white'
        })
      }
      else {
        let domitem = document.getElementById(event.target.id)
        //console.log(domitem)
        domitem.style.backgroundColor = 'black'
        domitem.style.color = 'white'
      }
    },
    leave(state, event){
      let idnum = event.target.id.split('-')[2]
      if (idnum) { //> sidenav or homenav
        let targets = [`grid-sidenav-${idnum}`, `grid-homenav-${idnum}-ico`, `grid-homenav-${idnum}-txt`]
        targets.forEach(element=>{
          let domitem = document.getElementById(element)
          //console.log(domitem)
          domitem.style.backgroundColor = 'white'
          domitem.style.color = 'black'
        })
      }
      else {
        let domitem = document.getElementById(event.target.id)
        //console.log(domitem)
        domitem.style.backgroundColor = 'white'
        domitem.style.color = 'black'
      }
    },
    clicked(state, event){

      state.active.now = event.target.id

      let className = event.target.parentNode.className.slice(0,9)
      let page = document.getElementById(event.target.id)
      let homenav = null

      if (event.target.id in state.pages) {

        console.log(event.target.id)

      } else {

        state.pages[event.target.id]=page
        console.log(state.pages[event.target.id])

      }

      if ( className === 'grid-topn' || className === 'grid-side' || className === 'grid-home' || event.target.id === 'private_collection' ){

        // deactive homenav
        console.log(className)
        let elm = document.getElementsByClassName('grid-homenav')
        console.log(elm)
        elm[0].style.display = 'none'

        // store it to re-display
        if (state.active.elm === null){
          // needed to reactivate
          state.active.elm = elm[0]
        }

        homenav = 'none'

      } else if ( className === 'grid-topb' )  {

        // reactive homenav
        state.active.elm.style.display = 'grid'
        homenav = 'grid'

      } else {
        state.active.elm.style.display = 'none'
        homenav = 'none'
      }



      // console.log(state.active.now)
      // console.log(state.pages[state.active.now])
      // console.log(state.pages)

    }
  },
  getters: {
    homenavobjs(state){
      let objs = []
      let idxs = [0,1,2,3,4,5,6,7]
      idxs.forEach(i => {
        objs.push({
          ico: state.grid.sidenav.content[i],
          txt: state.grid.homenav.content[i],
        })
      })
      // console.log(objs)
      return objs
    }
  }
})
